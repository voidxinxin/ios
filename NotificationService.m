//
//  NotificationService.m
//  FpPeriod_NotifServiceExtension
//
//  Created by zhangxin on 2021/4/29.
//

#import "NotificationService.h"

#define FPLogDebug(format, ...) NSLog((@"%s," "[FPNotificationService][lineNum:%d]" format) , __FUNCTION__, __LINE__, ##__VA_ARGS__);

@interface NotificationService ()

@property (nonatomic, strong) void (^contentHandler)(UNNotificationContent *contentToDeliver);
@property (nonatomic, strong) UNMutableNotificationContent *bestAttemptContent;

@end

@implementation NotificationService

- (void)didReceiveNotificationRequest:(UNNotificationRequest *)request withContentHandler:(void (^)(UNNotificationContent * _Nonnull))contentHandler {
    self.contentHandler = contentHandler;
    self.bestAttemptContent = [request.content mutableCopy];
    UNNotificationContent *content = [request content];
    NSDictionary *userInfo = [content userInfo];
    if (userInfo != nil) {
        NSDictionary *extraDic = [userInfo objectForKey:@"extra"];
        if (extraDic != nil) {
            NSString *mediaUrl = [extraDic objectForKey:@"imageUrl"];
            if (mediaUrl && mediaUrl.length > 0) {
                [self loadAttachmentForUrlString:mediaUrl withType:@"image" completionHandle:^(UNNotificationAttachment *attach) {
                    if (attach) {
                        self.bestAttemptContent.attachments = [NSArray arrayWithObject:attach];
                    }
                    self.contentHandler(self.bestAttemptContent);
                }];
                return;
            }
        }
    }
    
    self.contentHandler(self.bestAttemptContent);
}

- (void)serviceExtensionTimeWillExpire {
    // Called just before the extension will be terminated by the system.
    // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
    self.contentHandler(self.bestAttemptContent);
}
- (void)loadAttachmentForUrlString:(NSString *)urlStr

                          withType:(NSString *)type

                  completionHandle:(void(^)(UNNotificationAttachment *attach))completionHandler{
    
    __block UNNotificationAttachment *attachment = nil;
    
    NSURL *attachmentURL = [NSURL URLWithString:urlStr];
    
    NSString *fileExt = [self fileExtensionForMediaType:type];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [[session downloadTaskWithURL:attachmentURL
      
                completionHandler:^(NSURL *temporaryFileLocation, NSURLResponse *response, NSError *error) {
                    
                    if (error != nil) {
                        FPLogDebug(@"%@", error.localizedDescription);
                        
                    } else {
                        
                        NSFileManager *fileManager = [NSFileManager defaultManager];
                        
                        NSURL *localURL = [NSURL fileURLWithPath:[temporaryFileLocation.path stringByAppendingString:fileExt]];
                        
                        [fileManager moveItemAtURL:temporaryFileLocation toURL:localURL error:&error];
                        
                        NSError *attachmentError = nil;
                        
                        attachment = [UNNotificationAttachment attachmentWithIdentifier:[NSString stringWithFormat:@"FPNotification-%@",type] URL:localURL options:nil error:&attachmentError];
                        
                        if (attachmentError) {
                            
                            FPLogDebug(@"%@", attachmentError.localizedDescription);
                            
                        }
                        
                    }
                    
                    completionHandler(attachment);
                    
                }] resume];
    
}

- (NSString *)fileExtensionForMediaType:(NSString *)type {
    
    NSString *ext = type;
    
    if ([type isEqualToString:@"image"]) {
        
        ext = @"jpg";
        
    }
    
    if ([type isEqualToString:@"video"]) {
        
        ext = @"mp4";
        
    }
    
    if ([type isEqualToString:@"audio"]) {
        
        ext = @"mp3";
        
    }
    
    return [@"." stringByAppendingString:ext];
    
}
@end
